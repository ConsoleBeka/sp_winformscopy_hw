﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace WinFormsCopy
{
  public partial class Form1 : Form
  {
    public Form1()
    {
      InitializeComponent();
      btnResume.Enabled = false;
    }

    private void btnSourceFile_Click(object sender, EventArgs e)
    {
      OpenFileDialog dlg = new OpenFileDialog();
      dlg.CheckFileExists = true;
      dlg.Multiselect = false;
      dlg.Title = "File Open";
      if (dlg.ShowDialog() == DialogResult.OK)
      {
        txtSourceFile.Text = dlg.FileName;
      }
    }
    private void btnDestFile_Click(object sender, EventArgs e)
    {
      SaveFileDialog dlg = new SaveFileDialog();
      dlg.CheckFileExists = false;
      dlg.OverwritePrompt = true;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                txtDestFile.Text = dlg.FileName;
            }
            else
            {
                txtDestFile.Text = "";
            }
    }

    Thread CopyFile = null;
    CopyParam CopyFilePar = null;

    private void btnStart_Click(object sender, EventArgs e)
    {
      if (txtSourceFile.Text.Trim().Length == 0 ||
         txtDestFile.Text.Trim().Length == 0)
      {
        MessageBox.Show("Files are empty");
        return;
      }
      if (CopyFile != null)
      {
        return;
      }
      CopyFile = new Thread(ThCopyRoutine);
      CopyFile.IsBackground = true;
      CopyFilePar = new CopyParam();

      CopyFilePar.srcFileName =
                      txtSourceFile.Text.Trim();
      CopyFilePar.destFileName =
                      txtDestFile.Text.Trim();

      CopyFilePar.frm = this;
      pbFileCopy.Value = 0;
      pbFileCopy.Minimum = 0;    //   0 %
      pbFileCopy.Maximum = 1000; // 100 %
      pbFileCopy.Step = 100;     //  10 %

      CopyFile.Start(CopyFilePar);
    }

    int readSize = 1500;

    void ThCopyRoutine(object arg)
    {
      CopyParam par = arg as CopyParam;
      FileStream src =
        new FileStream(par.srcFileName,FileMode.Open,FileAccess.Read);
      FileStream dst = 
        new FileStream(par.destFileName,FileMode.OpenOrCreate,FileAccess.Write);
      //буффер для чтения/записи
      byte[] buf = new byte[readSize];

      FileInfo fi = new FileInfo(par.srcFileName);
      long fileSize = fi.Length;
      long readAll = 0;
      while (!par.IsStop)
      {
        int readBytes = src.Read(buf, 0, readSize);
        dst.Write(buf, 0, readBytes);
        //Число прочтенных байтов
        readAll += readBytes;
        int readProcent =
    (int)((double)readAll / fileSize * 100.0 * 10 + 0.5);
        par.frm.pbFileCopy.Invoke(new Action<int>(
          (x) => { par.frm.pbFileCopy.Value = x;
            par.frm.pbFileCopy.Update();
          }),
          readProcent);

        if(readAll == fileSize) {
          par.IsStop = true; 
        }
        par.evnPause.WaitOne();
      }
    }

    class CopyParam
    {
      public string srcFileName, destFileName;
      public Form1 frm;
      public bool IsStop;
      public ManualResetEvent evnPause;
      public CopyParam()
      {
        IsStop = false;
        evnPause = new ManualResetEvent(true);
      }
    }

    private void btnClose_Click(object sender, EventArgs e)
    {
      Application.Exit();
    }

    private void btnStop_Click(object sender, EventArgs e)
    {
      btnResume.Enabled = true;
      CopyFilePar.evnPause.Reset();
      CopyFilePar.IsStop = true;
    }

    private void btnResume_Click(object sender, EventArgs e)
    {
      CopyFilePar.evnPause.Set();
      CopyFilePar.IsStop = false;
      btnResume.Enabled = false;
    }
  }
}
